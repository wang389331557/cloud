from pyecharts.charts import Line
from pyecharts import options as opts

# 示例数据
cate = ['May 21 11:28:12','May 21 11:28:21','May 21 11:28:35','May 21 11:28:43','May 21 11:28:52','May 21 11:29:01','May 21 11:29:10','May 21 11:29:19','May 21 11:29:27']
data1 = [33539,36155,42143 ,46337 ,46667 ,47009 ,47405 ,47801 ,49165 ,49363]
data2 = [175326 ,179053 ,186164 ,190887 ,193190 ,195381 ,197818 ,200231 ,204030 ,206045 ]

"""
折线图示例:
1. is_smooth 折线 OR 平滑
2. markline_opts 标记线 OR 标记点
"""
line = (Line()
       .add_xaxis(cate)
       .add_yaxis('net_RX', data1,
                  markpoint_opts=opts.MarkPointOpts(data=[opts.MarkPointItem(name="自定义标记点",
                                                                             coord=[cate[2], data2[2]],
                                                                             value=data2[2])]))
       .add_yaxis('net_TX', data2,
                  is_smooth=True,
                  markpoint_opts=opts.MarkPointOpts(data=[opts.MarkPointItem(name="自定义标记点",
                                                                             coord=[cate[2], data2[2]], value=data2[2])]))
       .set_global_opts(title_opts=opts.TitleOpts(title="云平台实时数据", subtitle="网络数据"))
      )

line.render()
